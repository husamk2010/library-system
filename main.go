package main

import (
	_ "encoding/json"
	"fmt"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	_ "github.com/hkashko97/library-system/pkg/config"
	routes "github.com/hkashko97/library-system/pkg/routes"
)

func main() {
	// init the router
	router := mux.NewRouter()

	// attach the routes
	// controllers.AttachRoutes(router)
	routes.AttachBookRoutes(router)
	routes.AttachCategoryRoutes(router)

	// start the server
	fmt.Println("Server running on port: ", os.Getenv("PORT"))
	http.ListenAndServe(":"+os.Getenv("PORT"), router)
}
