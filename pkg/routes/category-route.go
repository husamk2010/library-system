package routes

import (
	"github.com/gorilla/mux"
	controllers "github.com/hkashko97/library-system/pkg/controllers"
)

func AttachCategoryRoutes(router *mux.Router) {
	router.HandleFunc("/categories", controllers.GetAllCategories).Methods("GET")
	router.HandleFunc("/categories", controllers.CreateCategory).Methods("POST")
}
