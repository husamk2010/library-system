package database

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var (
	db  *gorm.DB
	err error
)

func Open() (*gorm.DB, error) {

	db, err = gorm.Open(os.Getenv("DATABASE_DIALECT"), os.Getenv("DATABASE_URL"))

	if err != nil {
		fmt.Println("Failed to connect to database")
	}

	return db, err
}
