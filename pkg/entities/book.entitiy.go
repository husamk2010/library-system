package entities

import (
	"fmt"
)

type Book struct {
	ID       int64    `json:"id"`
	Title    string   `json:"title"`
	Price    float32  `json:"price"`
	Info     string   `json:"info"`
	Category Category `json:"category"`
	Author   []Author `json:"author"`
}

func (book *Book) Format() string {
	return fmt.Sprintf("=========\nID = %d\nTitle = %s\nPrice = %f\nInfo = %s\nCategory\n\tID = %d\n\tTitle = %s\n=========", book.ID, book.Title, book.Price, book.Info, book.Category.ID, book.Category.Title)
}