package entities

type Category struct {
	ID    int    `json:"id"`
	Title string `json:"title"`
}
