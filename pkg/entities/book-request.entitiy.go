package entities

type BookRequest struct {
	ID        int    `json:"id"`
	Book      Book   `json:"book"`
	User      User   `json:"user"`
	Status    string `json:"status"`
	StartDate string `json:"startDate"`
}
