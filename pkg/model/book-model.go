package model

import (
	entities "github.com/hkashko97/library-system/pkg/entities"
)

type Book struct {
	ID         int64 `gorm:"primaryKey"`
	Title      string
	Price      float32
	Info       string
	CategoryID int
	Category   Category
	Author     []Author `gorm:"many2many:book_authors;"`
}

func (book *Book) Create() entities.Book {
	db.Create(&book).Preload("Category").Find(&book)
	return book.serialize()
}

func (book *Book) Update(toUpdate *Book) entities.Book {
	db.Model(&book).Updates(&toUpdate)

	return book.serialize()
}

func GetAllBooks() (*[]entities.Book, error) {
	books := []Book{}
	err := db.Model(&Book{}).Preload("Category").Preload("Author").Find(&books).Error

	if err != nil {
		return nil, err
	}

	var result []entities.Book
	for _, item := range books {
		result = append(result, item.serialize())
	}

	return &result, nil
}

func GetBook(id int64) entities.Book {
	book := Book{ID: id}
	db.Where("ID=?", id).Find(&book)

	return book.serialize()
}

func DeleteBook(id int64) int64 {
	book := Book{ID: id}
	cur := db.Where("ID=?", id).Delete(&book)

	return cur.RowsAffected
}

func (mod *Book) serialize() entities.Book {

	book := entities.Book{
		ID:    mod.ID,
		Title: mod.Title,
		Price: mod.Price,
		Info:  mod.Info,
		Category: entities.Category{
			ID:    mod.Category.ID,
			Title: mod.Category.Title,
		},
	}

	if mod.Author != nil {
		var t []entities.Author

		for _, v := range mod.Author {
			t = append(t, entities.Author{
				ID:   v.ID,
				Name: v.Name,
			})
		}

		book.Author = t
	}

	return book
}
