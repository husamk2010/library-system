package model

import "github.com/hkashko97/library-system/pkg/entities"

type User struct {
	ID          int `gorm:"primaryKey"`
	FullName    string
	Email       string
	Password    string
	PhoneNumber string
}

func (user *User) serialize() entities.User {
	serializedUser := entities.User{
		ID:          user.ID,
		FullName:    user.FullName,
		Email:       user.Email,
		Password:    user.Password,
		PhoneNumber: user.PhoneNumber,
	}

	return serializedUser
}
