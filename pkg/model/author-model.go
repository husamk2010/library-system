package model

type Author struct {
	ID   int `gorm:"primaryKey"`
	Name string
}
