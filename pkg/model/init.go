package model

import (
	"log"

	"github.com/hkashko97/library-system/pkg/database"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var (
	db  *gorm.DB
	err error
)

func init() {
	db, err = database.Open()
	if err != nil {
		log.Fatal(err.Error())
	}

	// defer db.Close()

	db.AutoMigrate(&User{}, &Category{}, &Author{}, &Book{}, &BookRequest{})

	// authors := []Author{{Name: "Uncle Bob"}, {Name: "Martin mystery"}, {Name: "Kyne Stewart"}}

	// categories := []Category{{Title: "IT"}, {Title: "Health"}, {Title: "Commerce"}}

	// books := []Book{

	// 	{Title: "Book 1", Info: "Book 1 info", Price: 25, CategoryID: 1},
	// 	{Title: "Book 2", Info: "Book 2 info", Price: 51, CategoryID: 1},
	// 	{Title: "Book 3", Info: "Book 3 info", Price: 98, CategoryID: 2},
	// }

	// for _, item := range authors {
	// 	db.Create(&item)
	// }

	// for _, item := range categories {
	// 	db.Create(&item)
	// }

	// for _, item := range books {
	// 	db.Create(&item)
	// }

}
