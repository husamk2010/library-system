package model

import "github.com/hkashko97/library-system/pkg/entities"

type Category struct {
	ID    int `gorm:"primaryKey"`
	Title string
}

func (category *Category) Create() (entities.Category, error) {
	err := db.Create(&category).Error

	if err != nil {
		return entities.Category{}, err
	}
	return category.serialize(), nil
}

func GetAllCategories() ([]entities.Category, error) {
	var categories []Category
	err := db.Find(&categories).Error

	if err != nil {
		return []entities.Category{}, err
	}

	var result []entities.Category
	for _, cat := range categories {
		result = append(result, cat.serialize())
	}

	return result, nil
}

func (category *Category) serialize() entities.Category {
	mappedCategory := entities.Category{
		ID:    category.ID,
		Title: category.Title,
	}

	return mappedCategory
}
