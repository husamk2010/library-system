package model

import "github.com/hkashko97/library-system/pkg/entities"

type BookRequest struct {
	ID        int `gorm:"primaryKey"`
	Book      Book
	BookID    int
	User      User
	UserID    int
	Status    string
	StartDate string
}

var (
	Available = "Available"
	Booked    = "Booked"
)

func (bookRequest *BookRequest) serialize() entities.BookRequest {
	serializedBookRequest := entities.BookRequest{
		ID:        bookRequest.ID,
		Book:      bookRequest.Book.serialize(),
		User:      bookRequest.User.serialize(),
		Status:    bookRequest.Status,
		StartDate: bookRequest.StartDate,
	}

	return serializedBookRequest
}

func GetAllBookRequests() ([]entities.BookRequest, error) {
	var requests []entities.BookRequest
	var booksRequests []BookRequest

	err := db.Find(&booksRequests).Error

	if err != nil {
		return nil, err
	}

	for _, item := range booksRequests {
		requests = append(requests, item.serialize())
	}

	return requests, nil
}


func GetBookRequestByID(bookID int) (entities.BookRequest, error) {
	targetBook := BookRequest{ID: bookID}
	err := db.Preload("Book").Preload("User").Where("Book_ID = ? and Status = ?", bookID, Available).Last(&targetBook).Error

	if err != nil {
		return targetBook.serialize(), err
	}

	return targetBook.serialize(), nil
}
