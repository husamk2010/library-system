package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/hkashko97/library-system/pkg/model"
)

func GetAllBooks(w http.ResponseWriter, r *http.Request) {
	books, _ := model.GetAllBooks()

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&books)
}

func GetBookById(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(mux.Vars(r)["id"], 0, 0)

	fmt.Println("Received ID = ", id)
	if err != nil {
		fmt.Fprintf(w, "Invalid ID parameter")
		return
	}

	book := model.GetBook(id)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(&book)
}

func CreateBook(w http.ResponseWriter, r *http.Request) {

	var newBook model.Book
	json.NewDecoder(r.Body).Decode(&newBook)

	book := newBook.Create()

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)

	json.NewEncoder(w).Encode(&book)
}

func UpdateBook(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]

	bookId, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		fmt.Fprintf(w, "Invalid ID")
		return
	}

	var afterBook model.Book
	beforeBook := model.Book{ID: bookId}

	parseError := json.NewDecoder(r.Body).Decode(&afterBook)
	if parseError != nil {
		fmt.Fprintf(w, "Invalid inputs, please check again.")
		return
	}

	result := beforeBook.Update(&afterBook)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(result)

}

func DeleteBook(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)["id"]

	id, err := strconv.ParseInt(vars, 10, 64)
	if err != nil {
		fmt.Fprintf(w, "Invalid parameter")
		return
	}

	result := model.DeleteBook(id)

	if result > 0 {
		fmt.Fprintf(w, "Successfully deleted")
	} else {
		fmt.Fprintf(w, "Failed to delete")
	}
}
