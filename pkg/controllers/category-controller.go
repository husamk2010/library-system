package controllers

import (
	"encoding/json"
	"net/http"

	"github.com/hkashko97/library-system/pkg/model"
)

func GetAllCategories(w http.ResponseWriter, r *http.Request) {
	categories, _ := model.GetAllCategories()

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&categories)
}

func CreateCategory(w http.ResponseWriter, r *http.Request) {

	newCategory := model.Category{}
	json.NewDecoder(r.Body).Decode(&newCategory)

	newCategory.Create()

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)

	json.NewEncoder(w).Encode(&newCategory)
}
