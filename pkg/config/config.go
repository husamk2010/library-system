package config

import (
	"os"

	"github.com/joho/godotenv"
)

var ENV = make(map[string]string)

func init() {
	if err := godotenv.Load(".env"); err != nil {
		panic("Cant load the env")
	}

	ENV["PORT"] = os.Getenv("PORT")
}

func GetEnv() map[string]string {
	return ENV
}
